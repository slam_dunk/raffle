import './App.css';
import { useState, useRef, useEffect } from 'react';

const Numbers = (props) => {
  return <p class="text-9xl font-semibold">{props.content}</p>;
};

const Panel = (props) => {
  const total = props.poolSize;
  const delta = 10;
  const limit = props.prizeCount;

  const [removal, setRemoval] = useState("");

  const [state, setState] = useState({
    candidates: [...Array(total).keys()],
    num: total,
    winners: [],
  });

  const [pause, setPause] = useState(false);

  let intervalRef = useRef();

  const nextNum = () => setState((prev) => ({
    ...prev,
    num: prev.candidates[Math.floor(Math.random() * prev.candidates.length)]
  }));
  const onClick = () => {
    if (!pause) {
      clearInterval(intervalRef.current);
      setState((prev) => {
        if (prev.winners.length >= limit) {
          return prev;
        }
        return {
          ...prev,
          winners: [...prev.winners, prev.num],
          candidates: prev.candidates.filter((x) => x != prev.num)
        };
      });
    } else {
      intervalRef.current = setInterval(nextNum, delta);
    }
    setPause((prev) => !prev);
  };

  useEffect(() => {
    intervalRef.current = setInterval(nextNum, delta);

    return () => clearInterval(intervalRef.current);
  }, []);

  const onSubmit = (e) => {
    e.preventDefault();
    const form = e.target;
    const formData = new FormData(form);

    const number = parseInt(formData.get("removal"));
    if (isNaN(number)) {
      alert(`Please input a number to be removed.`);
      return;
    }
    if (state.winners.indexOf(number) <= -1) {
      alert(`Please input a number in the list.`);
      return;
    }
    setState((prev) => {
      return {
        ...prev,
        winners: prev.winners.filter((item) => item != number),
      };
    });
    setRemoval("");
  };
  return (
    <div>
      <h2 class="text-2xl">{props.title}</h2>
      <p class="text-sm">Tickets: {total}. Prizes: {limit}.</p>
      <div class="flex flex-col space-y-5">
        <Numbers content={state.num} />
        <div class="grid gap-6 md:grid-cols-12">
          <button class={
            "text-white bg-blue-500 hover:bg-blue-700 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 " +
            (state.winners.length >= limit ? "cursor-not-allowed opacity-50" : "")}
            onClick={onClick} disabled={state.winners.length >= limit}>
            {pause ? "Run" : "Pause"}
          </button>
        </div>
        <Numbers content={state.winners.join(' ')} />
        <form method="post" onSubmit={onSubmit} autocomplete="off">
          <div class="grid gap-6 mb-6 md:grid-cols-12">
            <input value={removal} name="removal" onChange={e => setRemoval(e.target.value)} class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" />
            <button class="text-white bg-red-500 hover:bg-red-700 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Remove</button>
          </div>
        </form>
      </div>
    </div>
  )
};

function App() {
  const [panels, setPanels] = useState([]);
  const [title, setTitle] = useState('');
  const [poolSize, setPoolSize] = useState('100');
  const [prizeCount, setPrizeCount] = useState('10');
  const listItems = panels.map((panel) => <div>{panel}</div>);
  const onSubmit = (e) => {
    e.preventDefault();
    const form = e.target;
    const formData = new FormData(form);

    const title = formData.get("title");
    if (!title) {
      alert(`Please input an title.`);
      return;
    }
    const poolSize = parseInt(formData.get('poolSize'));
    if (isNaN(poolSize)) {
      alert(`Please input a number for tickets.`);
      return;
    }
    if (poolSize >= 1000 || poolSize <= 0) {
      alert(`Please input the number of tickets between 1 - 999.`);
      return;
    }
    const prizeCount = parseInt(formData.get('prizeCount'));
    if (isNaN(prizeCount)) {
      alert(`Please input a number for prizes.`);
      return;
    }
    if (prizeCount > poolSize || prizeCount <= 0) {
      alert(`Please input the number of prizes between 1 - tickets`);
      return;
    }

    setPanels((panels) => [...panels, <Panel title={title} poolSize={poolSize} prizeCount={prizeCount} />]);
    setTitle("");
    setPoolSize(poolSize.toString());
    setPrizeCount("10");
  };
  return (
    <div>
      <nav class="flex iterms-center justify-between flex-wrap bg-gray-50 p-6">
        <div class="flex items-center flex-shrink-0 text-black mr-6">
          <span class="font-semibold text-xl tracking-tight"> Raffle! </span>
        </div>
        <div class="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
        </div>
      </nav>
      <div class="p-5 bg-white antialiased">
        <div class="divide-y divide-solid">
          {listItems}
        </div>
        <form method="post" onSubmit={onSubmit} autocomplete="off">
          <div class="grid gap-6 mb-6 md:grid-cols-6">
            <div>
              <label for="title" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Title
              </label>
              <input id="title" type="text" value={title} name="title" onChange={e => setTitle(e.target.value)} class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" />
            </div>
            <div>
              <label for="poolSize" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Tickets
              </label>
              <input id="poolSize" value={poolSize} name="poolSize" onChange={e => setPoolSize(e.target.value)} class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" />
            </div>
            <div>
              <label for="prizeCount" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Prizes
              </label>
              <input id="prizeCount" value={prizeCount} name="prizeCount" onChange={e => setPrizeCount(e.target.value)} class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" />
            </div>
          </div>
          <button class="text-white bg-blue-500 hover:bg-blue-700 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800" type="submit">
            Add
          </button>
        </form>
      </div>
    </div>
  )
}

export default App;
